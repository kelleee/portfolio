// set up =====
var express = require('express');
var app		= express();
var mongoose = require('mongoose');
var morgan	= require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');


// configuration
mongoose.connect('mongodb://kellyyai:admin@olympia.modulusmongo.net:27017/xuvOx3oq');
app.use(express.static(__dirname + '/public'));			// set up static files location
app.use(morgan('dev'));									// log every request to the console
app.use(bodyParser.urlencoded({'extended': 'true'}));	// parse application/x-www-form-urlencoded
app.use(bodyParser.json());								// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json'}));	// parse application/vnd.api+json as json
app.use(methodOverride());


// define model =====
var Todo = mongoose.model('Todo', {
	text: String
});

var Project = mongoose.model('Project', {
    title: String,
    type: String,
    description: String,
    date: String,
    language: String,
    tech: String,
    img: String,
    video: String,
    url: String,
    createdAt: Date
});


// routes =====
app.get('/api/todos', function(req, res) {

    // use mongoose to get all todos in the database
    Todo.find(function(err, todos) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err)
            res.send(err)

        res.json(todos); // return all todos in JSON format
    });
});

app.get('/api/projects', function(req, res) {
    Project.find(function(err, projects) {
        if (err)
            res.send(err);

        res.json(projects);
    })
});

app.get('/api/projects/:project_id', function(req, res) {
    Project.find({
        _id: req.params.project_id
    }, function(err, projects) {
        if (err)
            res.send(err);

        res.json(projects);
    });
});

app.post('/api/projects', function(req, res) {
    Project.create({
        title : req.body.title,
        type: req.body.type,
        description : req.body.description,
        date : req.body.date,
        language : req.body.language,
        tech : req.body.tech,
        img: req.body.img,
        video: req.body.video,
        url : req.body.url,
        createdAt : new Date(),
        done : false
    }, function(err, project) {
        if (err)
            res.send(err);

        Project.find(function(err, projects) {
            if (err)
                res.send(err)
            res.json(projects);
        });
    });
});

app.delete('/api/projects/:project_id', function(req, res) {
    Project.remove({
        _id: req.params.project_id
    }, function(err, todo) {
        if (err)
            res.send(err);

        Project.find(function(err, projects) {
            if (err)
                res.send(err)
            res.json(projects);
        });
    });
});


// create todo and send back all todos after creation
app.post('/api/todos', function(req, res) {

    // create a todo, information comes from AJAX request from Angular
    Todo.create({
        text : req.body.text,
        done : false
    }, function(err, todo) {
        if (err)
            res.send(err);

        // get and return all the todos after you create another
        Todo.find(function(err, todos) {
            if (err)
                res.send(err)
            res.json(todos);
        });
    });

});

// delete a todo
app.delete('/api/todos/:todo_id', function(req, res) {
    Todo.remove({
        _id : req.params.todo_id
    }, function(err, todo) {
        if (err)
            res.send(err);

        // get and return all the todos after you create another
        Todo.find(function(err, todos) {
            if (err)
                res.send(err)
            res.json(todos);
        });
    });
});


// application -----
app.get('', function(req, res) {
	res.sendfile('./public/index.html'); // load the single view file
})


// listen (start app with node server.js) =====
app.listen(8080);
console.log("App listening on port 8080");